package com.lh.project.mvc.test.dao.impl;

import com.lh.project.mvc.test.dao.WelcomeDao;
import com.lh.project.mvc.test.form.UserForm;

public class WelcomeDaoImpl implements WelcomeDao {

	@Override
	public UserForm login(String userName) {
		UserForm user=null;
		if("LH".equals(userName)){
			user=new UserForm();
			user.setName("LH");
			user.setAge(25);
		}
		return user;
	}

}
