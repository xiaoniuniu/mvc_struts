package com.lh.project.mvc.test.dao;

import com.lh.project.mvc.test.form.UserForm;

public interface WelcomeDao {
	
	UserForm login(String userName);

}
