package com.lh.project.mvc.test.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lh.project.mvc.mystruts.action.MyAction;
import com.lh.project.mvc.mystruts.form.FormBean;
import com.lh.project.mvc.test.form.UserForm;

public class WelcomeAction implements MyAction {

	@Override
	public String excute(FormBean form, HttpServletRequest request,
			HttpServletResponse response) {
			return "success";
			
	}

}
