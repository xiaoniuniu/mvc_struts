package com.lh.project.mvc.test.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lh.project.mvc.mystruts.action.MyAction;
import com.lh.project.mvc.mystruts.form.FormBean;
import com.lh.project.mvc.test.form.UserForm;
import com.lh.project.mvc.test.service.WelcomeService;
import com.lh.project.mvc.test.service.impl.WelcomeServiceImpl;

public class LoginAction implements MyAction {
	
	
	WelcomeService  welcomeService=new WelcomeServiceImpl() ;

	@Override
	public String excute(FormBean form, HttpServletRequest request,
			HttpServletResponse response) {
		UserForm param=(UserForm)form;
		UserForm user=welcomeService.login(param.getName());
		if(user!=null){
			request.setAttribute("user",user);
			return "success";
		}else{
			return "error";
		}
	
	}

}
