package com.lh.project.mvc.test.form;

import com.lh.project.mvc.mystruts.form.FormBean;

/**
 * @description(继承RequestForm 转换页面参数)
 * 
 * @author Liuhuan(liuhuan3611@163.com)
 * @version 2014-8-17 下午5:11:16
 */
public class UserForm implements FormBean {

	private String name;

	private Integer age;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

}
