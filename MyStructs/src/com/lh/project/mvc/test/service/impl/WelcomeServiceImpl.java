package com.lh.project.mvc.test.service.impl;

import com.lh.project.mvc.test.dao.WelcomeDao;
import com.lh.project.mvc.test.dao.impl.WelcomeDaoImpl;
import com.lh.project.mvc.test.form.UserForm;
import com.lh.project.mvc.test.service.WelcomeService;

public class WelcomeServiceImpl implements WelcomeService {

	
	WelcomeDao  welcomeDao =new WelcomeDaoImpl();
	
	@Override
	public UserForm login(String userName) {
		return welcomeDao.login(userName);
	}
	
	

}
