package com.lh.project.mvc.test.service;

import com.lh.project.mvc.test.form.UserForm;

public interface WelcomeService { 
	UserForm login(String userName);
}
