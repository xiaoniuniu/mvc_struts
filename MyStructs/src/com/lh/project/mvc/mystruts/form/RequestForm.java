package com.lh.project.mvc.mystruts.form;

import javax.servlet.http.HttpServletRequest;

public interface RequestForm {
  
	 public FormBean convertRequest2Bean(String className,HttpServletRequest request);
	
}
