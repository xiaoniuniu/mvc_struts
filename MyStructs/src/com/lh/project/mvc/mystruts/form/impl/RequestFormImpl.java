package com.lh.project.mvc.mystruts.form.impl;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import com.lh.project.mvc.mystruts.form.FormBean;
import com.lh.project.mvc.mystruts.form.RequestForm;

/**
 * @description(实现转换的实现)
 * 
 * @author Liuhuan(liuhuan3611@163.com)
 * @version 2014-8-17 下午5:16:38
 */
public class RequestFormImpl implements RequestForm {

	/**
	 * 利用反射把request的参数转换成formbean
	 */
	public FormBean convertRequest2Bean(String className,
			HttpServletRequest request) {
		try {
			if(className==null||"".equals(className)){
				return null;
			}
			
			Class<?> clazz = Class.forName(className);
			FormBean form = (FormBean) clazz.newInstance();

			Method[] methods = clazz.getDeclaredMethods();

			Enumeration params = request.getParameterNames();
			for (; params.hasMoreElements();) {

				String fieldName = params.nextElement().toString();
				// 得到字段
				Field field = clazz.getDeclaredField(fieldName);
				// 字段set方法名称
				String fieldMethodName = "set"
						+ fieldName.substring(0, 1).toUpperCase()
						+ fieldName.substring(1);

				Method setMethod = clazz.getMethod(fieldMethodName,
						new Class[] { field.getType() });
				// 调用set方法
				setMethod.invoke(form, request.getParameter(fieldName));
				
			}
			return form;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
