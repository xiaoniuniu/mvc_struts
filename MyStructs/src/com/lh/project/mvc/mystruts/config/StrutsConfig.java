package com.lh.project.mvc.mystruts.config;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.lh.project.mvc.mystruts.xml.XmlBean;

public class StrutsConfig {

	/**
	 * @description (解析Structs配置文件)
	 * @param filePath
	 * @auther Liuhuan(liuhuan3611@163.com)
	 * @date 2014-8-17 下午6:49:37
	 */
	public static Map<String, XmlBean> parserStrutsConfig(String filePath) {

		SAXBuilder builder = new SAXBuilder();
		// 用来存放请求url以及对应的处理类和跳转页面
		Map<String, XmlBean> xmlMap = new HashMap<String, XmlBean>();

		try {
			Document doc = builder.build(new File(filePath));
			Element root = doc.getRootElement();
			Element actionEle = root.getChild("action-mappings");
			Element formEle=root.getChild("formbeans");
			List<Element> listEle = actionEle.getChildren();
			List<Element> listFormEle=formEle.getChildren();
			for (Element e : listEle) {

				XmlBean xmlObj = new XmlBean();
				// id
				String id = e.getAttributeValue("id");
								
				// 对应的beanform
				String beanform = e.getAttributeValue("beanForm");
				for(Element ele : listFormEle){
					String beanId=ele.getAttributeValue("id");
					if(beanId.equals(beanform)){
						xmlObj.setFormClass(ele.getAttributeValue("class"));
						break;
					}
				}
				// 对应的actionClass
				String actionClass = e.getAttributeValue("class");
				xmlObj.setActionClass(actionClass);
				// 请求Url
				String url = e.getAttributeValue("url");
				//默认是不加后缀 去掉后缀
				String [] urlArray=url.split(",");
				
				
				// 得到跳转Ele
				List<Element> forwarEles = e.getChildren();
				Map<String, String> forwardMap = new HashMap<String, String>();

				// 遍历forward
				for (Element fowardEle : forwarEles) {
					String result = fowardEle.getAttributeValue("result");
					String value = fowardEle.getText();
					forwardMap.put(result, value);
				}
				xmlObj.setForward(forwardMap);
				xmlMap.put(urlArray[0], xmlObj);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return xmlMap;

	}

}
