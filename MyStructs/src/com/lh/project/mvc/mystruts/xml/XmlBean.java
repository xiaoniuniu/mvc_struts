package com.lh.project.mvc.mystruts.xml;

import java.io.Serializable;
import java.util.Map;

/**
 * @description(装载struts-config.xml配置文件的bean)
 * 
 * @author Liuhuan(liuhuan3611@163.com)
 * @version 2014-8-17 下午6:22:43
 */
public class XmlBean  implements Serializable {

	private static final long serialVersionUID = -5062580593909492058L;

	//用来存放请求参数formClass
	String formClass;

	//用来存放处理请求的ActionClass
	String actionClass;

	// save actionUrl-viewPath
	Map<String, String> forward;

	public String getFormClass() {
		return formClass;
	}

	public void setFormClass(String formClass) {
		this.formClass = formClass;
	}

	public String getActionClass() {
		return actionClass;
	}

	public void setActionClass(String actionClass) {
		this.actionClass = actionClass;
	}

	public Map<String, String> getForward() {
		return forward;
	}

	public void setForward(Map<String, String> forward) {
		this.forward = forward;
	}

}
