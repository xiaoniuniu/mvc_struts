package com.lh.project.mvc.mystruts.action;

import java.io.IOException;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lh.project.mvc.mystruts.form.FormBean;
import com.lh.project.mvc.mystruts.form.RequestForm;
import com.lh.project.mvc.mystruts.form.impl.RequestFormImpl;
import com.lh.project.mvc.mystruts.xml.XmlBean;



/**
 * @description(请求转发器)
 *
 * @author Liuhuan(liuhuan3611@163.com)
 * @version 2014-8-17  下午7:09:59
 */
public class MyDispatcherServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try{
			// 处理请求路径
			String url = getActionUrl(request.getServletPath());
			Map<String,XmlBean> xmlMap=(Map<String, XmlBean>) request.getServletContext().getAttribute("struts");
			
			XmlBean xml=xmlMap.get(url);
			RequestForm requestForm = new RequestFormImpl();
			// 参数转为bean
			FormBean bean =requestForm.convertRequest2Bean(xml.getFormClass(), request);
			
			//得到action实例
			Class<?> actionClazz=Class.forName(xml.getActionClass());
			MyAction action =(MyAction) actionClazz.newInstance();
			String result=action.excute(bean, request, response);
			
			String forwarUrl=xml.getForward().get(result);
			// 转发
			RequestDispatcher dispatcher = request
					.getRequestDispatcher(forwarUrl);
			dispatcher.forward(request, response);
		}catch(Exception e){
			e.printStackTrace();
		}

	}

	public String getActionUrl(String servletPath) {
		String[] urlPath = servletPath.split("\\.");
		return urlPath[0];
	}

}
