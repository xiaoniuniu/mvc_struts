package com.lh.project.mvc.mystruts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lh.project.mvc.mystruts.form.FormBean;


/**
 * @description(Action�ӿ�)
 *
 * @author Liuhuan(liuhuan3611@163.com)
 * @version 2014-8-17  ����7:27:41
 */
public interface MyAction {
	
	public String excute(FormBean form,HttpServletRequest request,HttpServletResponse response);

}
