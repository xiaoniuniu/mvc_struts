package com.lh.project.mvc.mystruts.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.lh.project.mvc.mystruts.config.StrutsConfig;

/**
 * @description(实现自己的监听器)
 * 
 * @author Liuhuan(liuhuan3611@163.com)
 * @version 2014-8-17 下午6:15:06
 */
public class MyListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		ServletContext context = sce.getServletContext();
		
		
		String filePath=MyListener.class.getClassLoader().getResource(context
				.getInitParameter("struts")).getPath();
		//初始化配置文件
		context.setAttribute("struts", StrutsConfig.parserStrutsConfig(filePath));

		System.out.println("初始化完成");
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		System.out.println("销毁完成");
	}

}
